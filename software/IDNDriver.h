#ifndef IDNDR_H
#define IDNDR_H

#include <stdint.h>
#include <bcm2835.h>
#include "IDNNode.h"

/*
Default Clock Divider
64 = 6.25 Mhz
32 = 12.5 Mhz
16 = 25 Mhz
8 = 50 MHz
*/

#define STAT_LIST_SIZE 1000
#define statPerk 1
#define DAC_WAVE_BUF_SIZE 99

uint16_t spiSpeedClockDiv;

int driverThreadCancelReq;  

int driverActive;


typedef struct {
  uint8_t data[20];
} DACPoint;


typedef struct {
  DACPoint* data;
  size_t len;
  uint32_t pDelay;
  int shutter;
  int once;
} DACWaveChunk;



void* driverThreadStart(void* thread_arg);
int driverQueueFrame(ISPFrame* frame);
void markChClose();

#endif 
