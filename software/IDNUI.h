#ifndef IDNUI_H
#define IDNUI_H

#include <bcm2835.h>

#include "IDNMain.h"

#define GPIO_DISP_LED RPI_V2_GPIO_P1_33

#define GPIO_BTN_0 RPI_V2_GPIO_P1_35
#define GPIO_BTN_1 RPI_V2_GPIO_P1_37
#define GPIO_BTN_2 RPI_V2_GPIO_P1_38
#define GPIO_BTN_3 RPI_V2_GPIO_P1_40

#define BCKL_ON_TIME 10

#define BTN_BCK 1
#define BTN_SET 2
#define BTN_UP 4
#define BTN_DOWN 8


#define UI_FSM_START 0x00
#define UI_FSM_LOAD 0x01
#define UI_FSM_IP 0x02
#define UI_FSM_FRAMES 0x03
#define UI_FSM_IP_SET_DHCP 0x04
#define UI_FSM_IP_SET_IP_A1 0x05
#define UI_FSM_IP_SET_IP_B1 0x06
#define UI_FSM_IP_SET_IP_C1 0x07
#define UI_FSM_IP_SET_IP_D1 0x08
#define UI_FSM_IP_SET_IP_A2 0x0a
#define UI_FSM_IP_SET_IP_B2 0x0b
#define UI_FSM_IP_SET_IP_C2 0x0c
#define UI_FSM_IP_SET_IP_D2 0x0d
#define UI_FSM_IP_SET_IP_A3 0x0e
#define UI_FSM_IP_SET_IP_B3 0x0f
#define UI_FSM_IP_SET_IP_C3 0x10
#define UI_FSM_IP_SET_IP_D3 0x11
#define UI_FSM_IP_SET_MASK 0x09

extern char   __BUILD_DATE;
extern char   __BUILD_NUMBER;


int initUI();

void updateUI();
void uiUpdateIP(uint32_t addr, uint32_t mask);

int frameCnt;
#endif
