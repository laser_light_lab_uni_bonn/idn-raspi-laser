#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>     /* defines STDIN_FILENO, system calls,etc */
#include <sys/types.h>  /* system data type definitions */
#include <sys/socket.h> /* socket specific definitions */
#include <netinet/in.h> /* INET constants and stuff */
#include <arpa/inet.h>  /* IP address conversion stuff */
#include <netdb.h>      /* gethostbyname */
#include <pthread.h>    /* POSIX Threads */
#include <string.h>


#include "IDNMain.h"
#include "IDNNode.h"
#include "IDNDriver.h"
#include "IDNUI.h"

pthread_t network_thread = 0;

void stopNetworkThread() {
  networkThreadCancelReq = 1;
  pthread_join(network_thread, NULL);
}

void startNetworkThread() {
  networkThreadCancelReq = 0;
  //Network Thread works on IDN Channel 1 per default
  network_thread = 0;
  if (pthread_create(&network_thread, NULL, &networkThreadStart, NULL) != 0) {
   printf("ERROR CREATING NETWORK THREAD\n");
   exit(-1);
  }
}

			 


/* server main routine */

int main() {
 
  /* Startup Driver Thread */
  pthread_t driver_thread = 0;
  if (pthread_create(&driver_thread, NULL, &driverThreadStart, NULL) != 0) {
    printf("ERROR CREATING DRIVER THREAD\n");
    return -1;
  }
  
  /* Startup Net Thread */
  startNetworkThread();
  

  initUI();

   do {
    updateUI();
    delay(50);
  } while(1);
   /*
  //Read from Command Line unitl 'q' was read
   char* console_cmd[256];
   do { 
    fscanf(stdin, "%s", console_cmd);
    sleep(1);
  } while (1);
   */

  printf("Shutting Down\n");

  //Request Threads to stop
  driverThreadCancelReq = 1;
  networkThreadCancelReq = 1;
  //Wait for Thread Cleanup
  printf("Wait for Driver \n");
  pthread_join(driver_thread, NULL);
  printf("Wait for Network \n");
  pthread_join(network_thread, NULL);


  
  printf("Local Cleanup \n");
  
  return(0);
}
