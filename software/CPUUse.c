/*

 by Lanzelot at stackoverflow

 */

#include "CPUUse.h"


static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle, lastTotalIO;

void CPULoadInit(){
  FILE* file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow,
	   &lastTotalSys, &lastTotalIdle, &lastTotalIO);
  fclose(file);
}

double CPULoadGetCurrentValue(){
  double percent;
  FILE* file;
unsigned long long totalUser, totalUserLow, totalSys, totalIdle, totalIO, total;

  file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu %llu", &totalUser, &totalUserLow,
	   &totalSys, &totalIdle, &totalIO);
  fclose(file);

  if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
      totalSys < lastTotalSys || totalIdle < lastTotalIdle || totalIO < lastTotalIO){
    //Overflow detection. Just skip this value.
    percent = -1.0;
  }
  else{
    total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
      (totalSys - lastTotalSys) + (totalIO - lastTotalIO);
    percent = total;
    total += (totalIdle - lastTotalIdle);
    percent /= total;
    percent *= 100;
  }

  lastTotalUser = totalUser;
  lastTotalUserLow = totalUserLow;
  lastTotalSys = totalSys;
  lastTotalIdle = totalIdle;
  lastTotalIO = totalIO;

  return percent;
}
