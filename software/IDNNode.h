#ifndef IDNNODE_H
#define IDNNODE_H
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>

/*
Laser configuration
*/
#define ISP_DB25_RED_WAVELENGTH  0x27E
#define ISP_DB25_GREEN_WAVELENGTH 0x214
#define ISP_DB25_BLUE_WAVELENGTH 0x1CC

#define ISP_DB25_USE_U1 0
#define ISP_DB25_USE_U2 0
#define ISP_DB25_USE_U3 0
#define ISP_DB25_USE_U4 0
#define ISP_DB25_U1_WAVELENGTH 0x1BD
#define ISP_DB25_U2_WAVELENGTH 0x241
#define ISP_DB25_U3_WAVELENGTH 0x1E8

/*
End of Laser configuration
*/

#define IDN_DESCRIPTOR_NOP 0x00
#define IDN_DESCRIPTOR_DRAW_CONTROL_0 0x01
#define IDN_DESCRIPTOR_DRAW_CONTROL_1 0x02
#define IDN_DESCRIPTOR_X 0x03
#define IDN_DESCRIPTOR_Y 0x04
#define IDN_DESCRIPTOR_Z 0x05
#define IDN_DESCRIPTOR_COLOR 0x06
#define IDN_DESCRIPTOR_WAVELENGTH 0x07
#define IDN_DESCRIPTOR_INTENSITY 0x08
#define IDN_DESCRIPTOR_BEAM_BRUSH 0x09


#define IDN_HEADER_CC_MESSAGE 0x40
#define IDN_HEADER_CC_PINGREQUEST 0x08
#define IDN_HEADER_CC_PINGRESPONSE 0x9
#define IDN_HEADER_CC_SCANREQUEST 0x10
#define IDN_HEADER_CC_SCANRESPONSE 0x11
#define IDNCMD_SERVICEMAP_REQUEST           0x12    // Request for unit services
#define IDNCMD_SERVICEMAP_RESPONSE          0x13    // Map of supported services


#define IDN_CH_MSG_HEADER_CHUNK_TYPE_VOID 0x00
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_LASER_WAVE 0x01
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_LASER_FRAME_CHUNK 0x02
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_LASER_FRAME_FIRST_FRAG 0x03
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_OCTET_SEG 0x10
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_OCTET_STR 0x11
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_DIMMER_LVL 0x18
#define IDN_CH_MSG_HEADER_CHUNK_TYPE_LASER_FRAME_SQL_FRAG 0xc0

#define IDN_CH_MSG_HEADER_CNL_CCLF_BMASK 0x40
#define IDN_CH_MSG_HEADER_CNL_CH_BMASK 0x3F

#define IDN_CH_CONF_HEADER_CFL_SDM_BMASK 0x30
#define IDN_CH_CONF_HEADER_CFL_SDM_OFFSET 4
#define IDN_CH_CONF_HEADER_CFL_CLOSE_BMASK 0x02
#define IDN_CH_CONF_HEADER_CFL_ROUTING_BMASK 0x01

#define IDN_FRAME_SAM_HEADER_FLAGS_SCM_BMASK 0x30
#define IDN_FRAME_SAM_HEADER_FLAGS_SCM_OFFSET 4
#define IDN_FRAME_SAM_HEADER_FLAGS_ONCE_BMASK 0x01

#define IDN_TAG_CAT_BMASK 0xF000
#define IDN_TAG_CAT_OFFSET 12
#define IDN_TAG_SUB_BMASK 0x0F00
#define IDN_TAG_SUB_OFFSET 8
#define IDN_TAG_ID_BMASK 0x00F0
#define IDN_TAG_ID_OFFSET 4
#define IDN_TAG_PRM_BMASK 0x000F
#define IDN_TAG_WL_BMASK 0x03FF


typedef struct IDNDescriptorTag {
  uint8_t type; 
  uint8_t precision; 
  uint8_t scannerId; 
  uint16_t wavelength; 
  struct IDNDescriptorTag *next;
} IDNDescriptorTag;

typedef struct IDNChannel {
  uint8_t chId;
  int valid;
  struct sockaddr remote;
  int closed;
  uint8_t sdm;
  uint8_t serviceId;
  uint8_t serviceMode;
  struct timeval timeout;
  IDNDescriptorTag *descriptors;
} IDNChannel;

typedef struct ISPDB25Point {
  uint16_t x;
  uint16_t y;
  uint16_t r;
  uint16_t g;
  uint16_t b;
  uint16_t intensity;
  uint16_t shutter;
  uint16_t u1;
  uint16_t u2;
  uint16_t u3;
  uint16_t u4;
  struct ISPDB25Point *next;
} ISPDB25Point;

typedef struct ISPFrame {
  uint8_t scm; //Service Configuration Match
  uint32_t dur;
  size_t len;
  int once;
  int shutter;
  int isWave;
  ISPDB25Point *data;
} ISPFrame;

typedef struct IDNHeader {
  uint8_t cc;
  uint8_t flags;
  uint16_t seq;
} IDNHeader;

typedef struct IDNChannelMsgHeader {
  uint16_t size;
  uint8_t cnl;
  uint8_t chunkType;
  uint32_t timestamp;
} IDNChannelMsgHeader;

typedef struct IDNChannelConfHeader {
  uint8_t scwc;
  uint8_t cfl;
  uint8_t serviceId;
  uint8_t serviceMode;
} IDNChannelConfHeader;

typedef struct IDNFrameSampleHeader {
  uint8_t flags;
  uint32_t dur;
} IDNFrameSampleHeader;


typedef struct IDNPingResponsePacket {
  IDNHeader head;
} IDNPingResponsePacket;

typedef struct IDNScanResponse
{
    uint8_t structSize;                         // Size of this struct.
    uint8_t protocolVersion;                    // Upper 4 bits: Major; Lower 4 bits: Minor
    uint8_t status;                             // Unit and link status flags
    uint8_t reserved;
    uint8_t unitID[16];                         // [0]: Len, [1]: Cat, [2..Len]: ID, padded with '\0'
    uint8_t hostName[20];                       // Not terminated, padded with '\0'

} IDNScanResponse;

typedef struct IDNScanResponsePacket {
  IDNHeader head;
  IDNScanResponse data;
} IDNScanResponsePacket;
  

typedef struct _IDNHDR_SERVICEMAP_RESPONSE
{
    uint8_t structSize;                         // Size of this struct.
    uint8_t entrySize;                          // Size of an entry - sizeof(IDNHDR_SERVICEMAP_ENTRY)
    uint8_t relayEntryCount;                    // Number of relay entries
    uint8_t serviceEntryCount;                  // Number of service entries

    // Followed by the relay table (of relayEntryCount entries)
    // Followed by the service table (of serviceEntryCount entries)

} IDNHDR_SERVICEMAP_RESPONSE;


typedef struct _IDNHDR_SERVICEMAP_ENTRY
{
    uint8_t serviceID;                          // Service: The ID (!=0); Relay: Must be 0
    uint8_t serviceType;                        // The type of the service; Relay: Must be 0
    uint8_t flags;                              // Status flags and options
    uint8_t relayNumber;                        // Service: Root(0)/Relay(>0); Relay: Number (!=0)
    uint8_t name[20];                           // Not terminated, padded with '\0'

} IDNHDR_SERVICEMAP_ENTRY;


typedef struct IDNServicemapResponsePacket {
  IDNHeader head;
  IDNHDR_SERVICEMAP_RESPONSE map_response;
  IDNHDR_SERVICEMAP_ENTRY map_entry;
} IDNServicemapResponsePacket;
  






int processIDNPacket(char* buf, unsigned int len, int sd, struct sockaddr *remote, unsigned int addr_len);
void* networkThreadStart();


int networkThreadCancelReq;
#endif /* IDNNODE_H */
