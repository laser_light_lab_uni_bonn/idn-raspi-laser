#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <bcm2835.h>
#include <pthread.h>
#include <math.h>

#include <sched.h>
#include <string.h>
#include <sys/mman.h>

#include "IDNMain.h"
#include "IDNDriver.h"
#include "IDNNode.h"
#include "IDNUI.h"

#define SHUTTER_PIN RPI_V2_GPIO_P1_03 


DACPoint* voidFrame; //empty frame


ISPFrame* blankFrame;


int markClose;

pthread_mutex_t backBufferMutex;

pthread_mutex_t waveBufferMutex;


DACWaveChunk waveBuffer[DAC_WAVE_BUF_SIZE];
int waveBufHead = 0, waveBufTail = 0;
int waveBufUsage = 0;
int waveBufUnderrun = 0;
int waveMode = 0;



DACPoint* buffers[2]; //front and back buffer
int frontBufferIndex; //either 0 or 1
size_t bufLen[2]; //number of points in fuffer
uint32_t bufPointDelay[2]; //delay bewteen points in us
int bufSwapReq; //Set != 0 to request buffer swap
int bufOnce[2]; //Draw buffer once only 
int bufShut[2]; //Shutter value

//long tErr = 0; //Error in 10th of us

int frameDropCntA = 0;
int frameDropCntB = 0;
int frameDropCntA_acc = 0;
int frameDropCntB_acc = 0;


unsigned long statList[STAT_LIST_SIZE][2];
long tsum = 0;
int tcount = 0;

/*
 * creates the DAC commands from an ISPDB25Point
 */
void dacPoint(char* DACPoint, ISPDB25Point* ISPPoint){
  DACPoint[0] = 0x00;
  DACPoint[1] = 0x00 | ((ISPPoint->x & 0xf000) >> 12) & 0x0f;
  DACPoint[2] = ((ISPPoint->x & 0x0ff0) >> 4) & 0xff;
  DACPoint[3] = ((ISPPoint->x & 0x000f) << 4) & 0xff;

  DACPoint[4] = 0x00;
  DACPoint[5] = 0x10 | ((ISPPoint->y & 0xf000) >> 12) & 0x0f;
  DACPoint[6] = ((ISPPoint->y & 0x0ff0) >> 4) & 0xff;
  DACPoint[7] = ((ISPPoint->y & 0x000f) << 4) & 0xff;

  DACPoint[8] = 0x00;
  DACPoint[9] = 0x20 | ((ISPPoint->r & 0xf000) >> 12) & 0x0f;
  DACPoint[10] = ((ISPPoint->r & 0x0ff0) >> 4) & 0xff;
  DACPoint[11] = ((ISPPoint->r & 0x000f) << 4) & 0xff;

  DACPoint[12] = 0x00;
  DACPoint[13] = 0x30 | ((ISPPoint->g & 0xf000) >> 12) & 0x0f;
  DACPoint[14] = ((ISPPoint->g & 0x0ff0) >> 4) & 0xff;
  DACPoint[15] = ((ISPPoint->g & 0x000f) << 4) & 0xff;

  DACPoint[16] = 0x02;
  DACPoint[17] = 0x40 | ((ISPPoint->b & 0xf000) >> 12) & 0x0f;
  DACPoint[18] = ((ISPPoint->b & 0x0ff0) >> 4) & 0xff;
  DACPoint[19] = ((ISPPoint->b & 0x000f) << 4) & 0xff;
}




/*
 * driverQueueFrame(ISPFrame* frame)
 * queues a new frame and requests buffer swap
 * returns -1 if back buffer is being written
 * returns -2 if there is a frame in back buffer
 * returns 0 else 
 */
int driverQueueFrame(ISPFrame* frame) {

  driverActive = 1;

  if (frame->isWave) {
    //This is a wave Chunk
    if (waveBufUsage == 0) waveBufUnderrun++;	// global count for empty waveBuf / underrun
    if (waveBufUsage >= DAC_WAVE_BUF_SIZE) { //Buffer Full
      if (statPerk)
	frameDropCntA++;
      return -2;
    }


    if (waveBuffer[waveBufHead].data != NULL)
      free(waveBuffer[waveBufHead].data);
    waveBuffer[waveBufHead].data = malloc(frame->len * sizeof(DACPoint));
    waveBuffer[waveBufHead].once = frame->once;
    waveBuffer[waveBufHead].len = frame->len;
    waveBuffer[waveBufHead].pDelay = ((uint32_t) (frame->dur / (double)frame->len)) ; 
    waveBuffer[waveBufHead].shutter = frame->shutter;


    
    ISPDB25Point* last = NULL;
    ISPDB25Point* point = frame->data;
    int i = 0;
    while (point != NULL) {
      if (i >= frame->len) {
	printf("ERROR Frame length missmatch 1\n");
	break;
      }
      dacPoint(waveBuffer[waveBufHead].data[i].data, point);

      last = point;
      point = point->next;
      i++;
    }
    if (i != frame->len) {
      printf("ERROR Frame length missmatch 2\n");
    }
  
    
    waveMode = 1; //activate Wave Mode
    frameCnt++;
    pthread_mutex_lock(&waveBufferMutex);
    waveBufUsage++;
    waveBufHead = (waveBufHead + 1) % DAC_WAVE_BUF_SIZE;
    if(pthread_mutex_unlock(&waveBufferMutex)) return -3;
    return 0;
    
  } else {
    //THIS is not a wave chunk
    if (pthread_mutex_trylock(&backBufferMutex)) {
      if (statPerk)
	frameDropCntB++;
      return -1;
    }
  

    // This is a frame
    if (bufSwapReq) {
      if (statPerk)
	frameDropCntA++;
      pthread_mutex_unlock(&backBufferMutex);
      return -2;
    }
  
    int backBufferIndex = frontBufferIndex ^ 0x01;

    bufOnce[backBufferIndex] = frame->once;
    bufLen[backBufferIndex] = frame->len;
    bufPointDelay[backBufferIndex] = (uint32_t) (frame->dur / (double)frame->len) ;
    bufShut[backBufferIndex] = frame->shutter;

    free(buffers[backBufferIndex]); // free old back buffer
    buffers[backBufferIndex] = malloc(frame->len * sizeof(DACPoint));

  
    ISPDB25Point* last = NULL;
    ISPDB25Point* point = frame->data;
    int i = 0;
    while (point != NULL) {
      if (i >= frame->len) {
	printf("ERROR Frame length missmatch 1\n");
	break;
      }
      dacPoint(buffers[backBufferIndex][i].data, point);

      last = point;
      point = point->next;
      i++;
    }
    if (i != frame->len) {
      printf("ERROR Frame length missmatch 2\n");
    }
    
    waveMode = 0; //deactivate Wave Mode
    
    bufSwapReq = 1; //req buffer swap
    frameCnt++;
    if(pthread_mutex_unlock(&backBufferMutex)) return -3;
    return 0;
  }
}


void markChClose() {
  driverActive = 0;
  markClose = 1;
}

void processStat(long delay) {
  long max = 0, min = statList[0][0], sum = 0, hsum,  median, plus2x = 0, plus11x = 0;
  int i, j;
  for (i = 0; i < STAT_LIST_SIZE ; i++) {
    if (statList[i][1] == 0) break;
    if (statList[i][0] > max) max = statList[i][0];
    if (statList[i][0] < min) min = statList[i][0];
    if (statList[i][0] > delay*20) plus2x += statList[i][1]; //count points that were longer than 2x
    if (statList[i][0] > delay*11) plus11x += statList[i][1]; //count points that were longer than 1.1x
    // printf("%d | %d\n", statList[i][0], statList[i][1]);
    sum += statList[i][1];
  }
  
  hsum = (long) (sum / 2.0F);
  int index = 0;
  while (hsum > 0) {
    long min2 = max;

    for (j = 0; j < i; j++) { //find current minimum
      if (statList[j][0] <= min2 && statList[j][0] != 0) {
	min2 = statList[j][0];
	index = j;
      }
    }

    hsum -= statList[index][1];
    median = statList[index][0];
    statList[index][0] = 0; 
  }
  printf("median: %ld, min: %ld, max: %ld [100ns], %% samples >2.0x too long: %f %%, %% >1.1x too long: %f %%\n", median, min, max, (double) plus2x * 100 / (double) sum , (double) plus11x * 100  / (double) sum);



}


void driverRunLoop() {
  struct timespec now, pulse;
  uint32_t sdif, nsdif, tdif;


  DACPoint* b = buffers[frontBufferIndex]; //Current Front Buffer
  char* bp = NULL; //Current Point from Buffer
  long blen = bufLen[frontBufferIndex]; //Length of current Front Buffer
  unsigned long bpdelay = bufPointDelay[frontBufferIndex]; //Delay Bewteen Points in us for current Front Buffer
  int bshut = bufShut[frontBufferIndex]; //Shutter status for current front Buffer

   if (bufSwapReq) { //Buffer Swap Request present
	frontBufferIndex ^= 0x01; // Swap Buffer

	//Setup local cache
	b = buffers[frontBufferIndex]; 
	blen = bufLen[frontBufferIndex];
	bpdelay = bufPointDelay[frontBufferIndex];
	bshut = bufShut[frontBufferIndex];

	bcm2835_gpio_write(SHUTTER_PIN, bshut);
	bufSwapReq = 0; //Finish Request
  }

  
  int k = 0;
  while (1) {

    clock_gettime(CLOCK_MONOTONIC, &now);
    
    bp = b[k].data;
    bcm2835_spi_writenb(bp, 4); //Write X  Data
    bp += 4;
    bcm2835_spi_writenb(bp, 4); //Write Y Data
    bp += 4;
    bcm2835_spi_writenb(bp, 4); //Write R Data
    bp += 4;
    bcm2835_spi_writenb(bp, 4); //Write G Data
    bp += 4;
    bcm2835_spi_writenb(bp, 4); //Write B Data

    k++;
    if (k == blen) {
      k = 0;
    }

    //replaced k = (k+1) % blen;

    if (k == 0) {//restart frame
      if (driverThreadCancelReq) { //Cancel Driver Thread
	bcm2835_gpio_clr(SHUTTER_PIN);
	printf("Cancel\n");
	return;
      }

      //No new frame and channelo close req
      if (!bufSwapReq && markClose) {
	driverQueueFrame(blankFrame);
        driverActive = 0;
	markClose = 0;
      }

      if (waveMode) {
	while(waveBufUsage < 1 && !markClose && waveMode);
	if (markClose) {
	  driverQueueFrame(blankFrame);
          driverActive = 0;
	  markClose = 0;
	} else if (waveMode) {
     
	
	  b = waveBuffer[waveBufTail].data;
	  blen = waveBuffer[waveBufTail].len;
	  bpdelay = waveBuffer[waveBufTail].pDelay;
	  bshut = waveBuffer[waveBufTail].shutter;
	
	  //Open shutter
	  bcm2835_gpio_write(SHUTTER_PIN, bshut);


	  pthread_mutex_lock(&waveBufferMutex);
	  waveBufUsage--;
	  waveBufTail++;
	  if (waveBufTail ==  DAC_WAVE_BUF_SIZE)
	    waveBufTail = 0;
	  pthread_mutex_unlock(&waveBufferMutex); 
	}
      } else {
	
	if (bufSwapReq) { //Buffer Swap Request present
	  frontBufferIndex ^= 0x01; // Swap Buffer


	  //Setup local cache
	  b = buffers[frontBufferIndex]; 
	  blen = bufLen[frontBufferIndex];
	  bpdelay = bufPointDelay[frontBufferIndex];
	  bshut = bufShut[frontBufferIndex];
	
	  bufSwapReq = 0; //Finish Request
	  //Open shutter
	  bcm2835_gpio_write(SHUTTER_PIN, bshut);
	}
      }
    }

     //calculate speed adjustment factor
    double speedFactor = 1.0;
    if (waveMode) {
      if (waveBufUsage < 5) {
	speedFactor = 1.05;
      } else if (waveBufUsage >  10 && waveBufUsage <= 15){
	speedFactor = 0.9;
      } else if (waveBufUsage > 15) {
	speedFactor = 0.8;
      }
    }
    
    do { //Delays Until bpdelay is reached
      clock_gettime(CLOCK_MONOTONIC, &pulse);
      sdif = pulse.tv_sec - now.tv_sec;
      nsdif = pulse.tv_nsec - now.tv_nsec;
      tdif = sdif*1000000000 + nsdif ;
    } while (tdif < bpdelay * 1000 * speedFactor); //* speedFactor


    // tErr += bpdelay * 10 - (long) floor(tdif / 100.0);
    

    if (statPerk) { //if statistic collection actiavted
      unsigned long twait = (unsigned long) (tdif / 100.0F); //Time actually waited in 10th of us
      int i;
      for (i = 0; i < STAT_LIST_SIZE; i++) {
	if (statList[i][0] == 0 || statList[i][0] == twait) {//find empty line or match bucket
	  statList[i][0] = twait;
	  statList[i][1]++;
	  break;
	}
      }

      tsum += tdif;
      if (tsum >= 1e9) { //after collecting data for 1s
	tcount++;
	tsum = 0;
	processStat(bpdelay);			
	printf("[%ld] ", now.tv_sec);

	if ( !driverActive ) printf("___<IDLE>___ ");
	else if (waveMode) printf("_WAVE mode__ ");	// IDLE or frame/wave mode
		else printf("_FRAME mode_ ");

	printf("ChunkDrop A: %d/s, ChunkDrop B: %d/s, ", frameDropCntA, frameDropCntB);

	printf("Target diff between DAC samples %ld us - effective scanspeed %ld kpps\n", bpdelay, 1000 / (bpdelay));	

	frameDropCntA_acc += frameDropCntA;
	frameDropCntB_acc += frameDropCntB;
      	if (tcount >= 5) { // every 5s print global data
		tcount = 0;
		printf("###### accumulated values #############################\n");
		printf("## ChunkDrop A: %d, ChunkDrop B: %d \n", frameDropCntA_acc, frameDropCntB_acc);
		printf("## #waveBufUnderrun: %d \n", waveBufUnderrun);
		printf("#######################################################\n");

      	}



	if (waveMode) printf("BUsage: %d/%d chunks, ", waveBufUsage, DAC_WAVE_BUF_SIZE);
	else printf("Buffer Usage: %d frames, ", waveBufUsage);

	frameDropCntA = 0;
	frameDropCntB = 0;
	memset(statList, 0, sizeof(statList)); //reset stat array
      }
    }
  }


}

void* driverThreadStart(void* thread_arg) {
  printf("Start Driver\n");

  //This code prevents Memory Swapping and imporves performance 
  struct sched_param sp;
  memset(&sp, 0, sizeof(sp));
  sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
  sched_setscheduler(0, SCHED_FIFO, &sp);
  mlockall(MCL_CURRENT | MCL_FUTURE);
  //

  pthread_mutexattr_t mutexAttr;
  pthread_mutexattr_init(&mutexAttr);
  pthread_mutex_init (&backBufferMutex,&mutexAttr);


  pthread_mutexattr_init(&mutexAttr);
  pthread_mutex_init (&waveBufferMutex,&mutexAttr);
  
  
  int driverThreadCancelReq = 0;
  uint16_t spiSpeedClockDiv = BCM2835_SPI_CLOCK_DIVIDER_8;

  
  //Init BCM2835 Driver
  if (!bcm2835_init())
  {
    printf("bcm2835_init failed. Are you running as root??\n");
    bcm2835_close();
    return NULL;
  }


  //Init GPIO4 (Shutter)
  bcm2835_gpio_fsel(SHUTTER_PIN, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_set_pud(SHUTTER_PIN, BCM2835_GPIO_PUD_DOWN);
  bcm2835_gpio_clr(SHUTTER_PIN);


  //Enable SPI

  if (!bcm2835_spi_begin()) {
    printf("ERROR Setup SPI\n");
    bcm2835_spi_end();
    bcm2835_close();
    return NULL;
  }


  //Select SPI Channel
  bcm2835_spi_chipSelect(BCM2835_SPI_CS0); //CS 0
  //Set SPI Speed
  bcm2835_spi_setClockDivider(spiSpeedClockDiv); 

  
  //Setupp DAC for internal reference Votage and software LDAC
  unsigned char setupVREF[5] = {0x08, 0x00, 0x00, 0x00};
  unsigned char setupLDAC[5] = {0x06, 0x00, 0x00, 0x00};
  //  bcm2835_spi_writenb(setupVREF, 4);
  bcm2835_spi_writenb(setupLDAC, 4);

  blankFrame = malloc(sizeof(ISPFrame));
  ISPDB25Point* point = malloc(sizeof(ISPDB25Point));
  ISPDB25Point* point2 = malloc(sizeof(ISPDB25Point));
  
   
  point->x = 0x8000;
  point->y = 0x8000; 
  point->r = 0x0000;
  point->g = 0x0000;
  point->b = 0x0000;
  point->intensity = 0x0000;
  point->shutter = 0x0000;
  point->u1 = 0x0000;
  point->u2 = 0x0000;
  point->u3 = 0x0000;
  point->u4 = 0x0000;
  point->next = NULL;
  /*
  point2->x = 0x0000;
  point2->y = 0xffff; 
  point2->r = 0xffff;
  point2->g = 0x0000;
  point2->b = 0x8000;
  point2->intensity = 0x0000;
  point2->shutter = 0x0000;
  point2->u1 = 0x0000;
  point2->u2 = 0x0000;
  point2->u3 = 0x0000;
  point2->u4 = 0x0000;
  point2->next = NULL;

*/  
  
  blankFrame->once = 0;
  blankFrame->scm = 0;
  blankFrame->dur = 200;
  blankFrame->len = 1;
  blankFrame->data = point;
  blankFrame->shutter = 0;


  int i =  driverQueueFrame(blankFrame);
  if (i != 0) {
    printf("ERROR Init Blank Frame %i\n", i);
  }

  driverActive = 0;
  markClose = 1;

  printf("Driver Init Finished\n" );

  //Run Loop
  driverRunLoop();
 

  //Free BlankFrame
  free(blankFrame->data);
  free(blankFrame);
  
  pthread_mutex_destroy(&backBufferMutex);

  //DeInit
  bcm2835_spi_end();
  bcm2835_close();

  printf("Driver Stopped\n");
}
