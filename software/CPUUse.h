#ifndef CPUUSE_H
#define CPUUSE_H


#include "stdlib.h"
#include "stdio.h"
#include "string.h"

void CPULoadInit();
double CPULoadGetCurrentValue();

#endif
