#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>



#include "IDNUI.h"
#include "rpi_hd44780.h"
#include "CPUUse.h"

long bcklTime;
int bckl = 0;
int bcklTimeout = 10;

int blinkCnt = 0;
int blink = 0;
int blinkON = 5;
int blinkOFF = 20;



/*status*/
int dhcp = 1;
uint8_t ip[4] = {0, 0, 0, 0};
uint8_t netmask = 0;
float cpuLoad;


/*conf*/
int conf_dhcp;
uint8_t conf_ip[4];
uint8_t conf_netmask;
int lbtn = 0;



int fsmState = UI_FSM_START;



void uiUpdateIP(uint32_t addr, uint32_t mask) {
  ip[0] = (addr & 0xff000000) >> 24;
  ip[1] = (addr & 0xff0000) >> 16;
  ip[2] = (addr & 0xff00) >> 8;
  ip[3] = (addr & 0xff);
  
  int cidr = 0;
  while ( mask )
    {
      cidr += ( mask & 0x01 );
      mask >>= 1;
    }
  netmask = cidr;
}

int initDisplay() {
  if (lcd_set_gpio_pins()) {
    return 1;
  }
  lcd_init();
  
  lcd_clear_screen();
  lcd_set_cursor(LINE1_START);
  lcd_send_string("");
  lcd_set_cursor(LINE2_START);
  lcd_send_string("");
  return 0;
}

int initBckl() {
  if (!bcm2835_init()) {
    return 1;
  }

  bcm2835_gpio_fsel(GPIO_DISP_LED, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_write(GPIO_DISP_LED, 0);
  return 0;
}

int initBtn() {
  if (!bcm2835_init()) {
    return 1;
  }

  

  bcm2835_gpio_fsel(GPIO_BTN_0, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(GPIO_BTN_1, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(GPIO_BTN_2, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(GPIO_BTN_3, BCM2835_GPIO_FSEL_INPT);


  /* Using external PullDown
  bcm2835_gpio_set_pud(GPIO_BTN_0, BCM2835_GPIO_PUD_DOWN);
  bcm2835_gpio_set_pud(GPIO_BTN_1, BCM2835_GPIO_PUD_DOWN);
  bcm2835_gpio_set_pud(GPIO_BTN_2, BCM2835_GPIO_PUD_DOWN);
  bcm2835_gpio_set_pud(GPIO_BTN_3, BCM2835_GPIO_PUD_DOWN);
  */
  
  bcm2835_gpio_ren(GPIO_BTN_0);
  bcm2835_gpio_ren(GPIO_BTN_1);
  bcm2835_gpio_ren(GPIO_BTN_2);
  bcm2835_gpio_ren(GPIO_BTN_3);
}

void updateDisplay() {
  char str[2][20];
   sprintf(str[0],"");
   sprintf(str[1],"");
   
  switch (fsmState) {
  case UI_FSM_START:
    printf("t1");
    sprintf(str[0],"IDN Raspberry Pi");
    sprintf(str[1],"build:    0x%04x", &__BUILD_NUMBER);
    break;
  case UI_FSM_LOAD:
    sprintf(str[0],"1. CPU Load:");
    double cpuLoad = CPULoadGetCurrentValue();
    sprintf(str[1],"%5.2f%%", cpuLoad);
    break;
  case UI_FSM_IP:
    if (dhcp) {
      sprintf(str[0],"2. IP: DHCP  /%d", netmask);
    } else {
      sprintf(str[0],"2. IP: STAT  /%d", netmask);
    }
    sprintf(str[1],"%03d.%03d.%03d.%03d", ip[0], ip[1], ip[2], ip[3]);
    break;
  case UI_FSM_FRAMES:
    sprintf(str[0],"3. #Frames:");
    sprintf(str[1],"%d", frameCnt);
    break;
  case UI_FSM_IP_SET_DHCP:
    sprintf(str[0],"2.1 DHCP?:");
    if (conf_dhcp) {
      sprintf(str[1],"YES");
    } else {
      sprintf(str[1],"NO");
    }
    break;
  case UI_FSM_IP_SET_IP_A1:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1]," %02d.%03d.%03d.%03d", (conf_ip[0] % 100),conf_ip[1], conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_A2:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%01d %01d.%03d.%03d.%03d", (conf_ip[0] / 100), conf_ip[0] % 10,conf_ip[1], conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_A3:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%02d .%03d.%03d.%03d", (conf_ip[0] / 10),conf_ip[1], conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
   case UI_FSM_IP_SET_IP_B1:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d. %02d.%03d.%03d", conf_ip[0], (conf_ip[1] % 100), conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_B2:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%01d %01d.%03d.%03d", conf_ip[0], (conf_ip[1] / 100), conf_ip[1] % 10, conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_B3:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%02d .%03d.%03d", conf_ip[0], (conf_ip[1] / 10), conf_ip[2], conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
     case UI_FSM_IP_SET_IP_C1:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d. %02d.%03d", conf_ip[0], conf_ip[1], (conf_ip[2] % 100), conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_C2:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d.%01d %01d.%03d", conf_ip[0], conf_ip[1], (conf_ip[2] / 100), conf_ip[2] % 10, conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_C3:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d.%02d .%03d", conf_ip[0], conf_ip[1], (conf_ip[2] / 10),  conf_ip[3]);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
     case UI_FSM_IP_SET_IP_D1:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d.%03d. %02d", conf_ip[0], conf_ip[1], conf_ip[2], (conf_ip[3] % 100));
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_D2:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d.%03d.%01d %01d", conf_ip[0], conf_ip[1], conf_ip[2], (conf_ip[3] / 100), conf_ip[3] % 10);
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_IP_D3:
    sprintf(str[0],"2.2 IP?:");
    if (blink)  {
      sprintf(str[1],"%03d.%03d.%03d.%02d ", conf_ip[0],conf_ip[1], conf_ip[2], (conf_ip[3] / 10));
    } else {
      sprintf(str[1],"%03d.%03d.%03d.%03d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3]);
    }
    break;
  case UI_FSM_IP_SET_MASK:
    sprintf(str[0],"2.3 SUBNETMASK?:");
    sprintf(str[1],"/%d", conf_netmask );

    break;
  }
  lcd_clear_screen();
  lcd_set_cursor(LINE1_START);
  lcd_send_string(str[0]);
  lcd_set_cursor(LINE2_START);
  lcd_send_string(str[1]);
}


void bcklOn() {
  bcklTime = (long) time(NULL);
  if (!bckl) {  
    bcm2835_gpio_write(GPIO_DISP_LED, 1);
    bckl = 1;
  }
}

void bcklOff() {
  if (bckl) {
    bcm2835_gpio_write(GPIO_DISP_LED, 0);
    bckl = 0;
  }
}


void processBcklTimeout() {
  if (bcklTimeout && bckl) {
    long now = (long) time(NULL);
    if (now - bcklTime >= bcklTimeout)
      bcklOff();
  }
}


void processBlink() {
  blinkCnt++;
  if (blink) {
    if (blinkCnt >= blinkON) {
      blink = 0;
      blinkCnt = 0 ;
    }
  } else {
    if (blinkCnt >= blinkOFF) {
      blink = 1;
      blinkCnt = 0;
    }
  }
}

void applyNetworkSettings() {
  stopNetworkThread();
  if (conf_dhcp) {
    system("ifconfig eth0 0.0.0.0 && dhclient eth0");
    dhcp = 1;
  } else {
    unsigned char command[100];
    system("killall dhclient");
    sprintf((char*)command,"ifconfig eth0 %d.%d.%d.%d/%d", conf_ip[0], conf_ip[1], conf_ip[2], conf_ip[3], conf_netmask);

    system(command);
    dhcp = 0;
  }
  startNetworkThread();
}


void processBTN(int btn) {
  int bl = bckl; 
  bcklOn();
  if (!bl) {
    updateDisplay();
    return;
  }
  lbtn = btn;
  switch (fsmState) {
    case UI_FSM_START:
      if (btn == BTN_DOWN) {
        fsmState = UI_FSM_LOAD;
      }
      break;
    case UI_FSM_LOAD:
      if (btn == BTN_DOWN) {
	fsmState = UI_FSM_IP;
      } else if (btn == BTN_UP) {
	fsmState = UI_FSM_START;
      }
      break;
    case UI_FSM_IP:
      if (btn == BTN_DOWN) {
	fsmState = UI_FSM_FRAMES;
      } else if (btn == BTN_UP) {
	fsmState = UI_FSM_LOAD;
      } else if (btn == BTN_SET) {
	//copy network configuration into conf variables
	conf_dhcp = dhcp;
	conf_ip[0] = ip[0];
	conf_ip[1] = ip[1];
	conf_ip[2] = ip[2];
	conf_ip[3] = ip[3];
	conf_netmask = netmask;
	
	fsmState = UI_FSM_IP_SET_DHCP;
      }
      break;
    case UI_FSM_FRAMES:
      if (btn == BTN_UP) {
	fsmState = UI_FSM_IP;
      }
      break;
    case UI_FSM_IP_SET_DHCP:
      if (btn == BTN_SET) {
	if (conf_dhcp) {
	  //save settings
	  applyNetworkSettings();
	  fsmState = UI_FSM_IP;
	} else {
	  fsmState = UI_FSM_IP_SET_IP_A1;
	}
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP;
      } else if (btn == BTN_UP || btn == BTN_DOWN) {
	conf_dhcp ^= 0x01;
      }
      break;
    case UI_FSM_IP_SET_IP_A1:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_A2;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_DHCP;
      } else if (btn == BTN_UP) {
	if (conf_ip[0] <= 155) {
	    conf_ip[0] += 100;
	} else
	  conf_ip[0] = 255;
      } else if (btn == BTN_DOWN) {
	if (conf_ip[0] >= 100) {
	    conf_ip[0] -= 100;
	} else
	  conf_ip[0] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_A2:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_A3;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_A1;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[0] <= 245) {
	    conf_ip[0] += 10;
	  } else
	    conf_ip[0] = 255;
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[0] >= 10) {
	    conf_ip[0] -= 10;
	  } else
	    conf_ip[0] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_A3:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_B1;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_A2;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[0] < 255) {
	    conf_ip[0] += 1;
	  }
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[0] > 0) {
	    conf_ip[0] -= 1;
	  }
      }
      break;
          case UI_FSM_IP_SET_IP_B1:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_B2;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_A3;
      } else if (btn == BTN_UP) {
	if (conf_ip[1] <= 155) {
	    conf_ip[1] += 100;
	} else
	  conf_ip[1] = 255;
      } else if (btn == BTN_DOWN) {
	if (conf_ip[1] >= 100) {
	    conf_ip[1] -= 100;
	} else
	  conf_ip[1] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_B2:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_B3;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_B1;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[1] <= 245) {
	    conf_ip[1] += 10;
	  } else
	    conf_ip[1] = 255;
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[1] >= 10) {
	    conf_ip[1] -= 10;
	  } else
	    conf_ip[1] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_B3:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_C1;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_B2;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[1] < 255) {
	    conf_ip[1] += 1;
	  }
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[1] > 0) {
	    conf_ip[1] -= 1;
	  }
      }
      break;
  case UI_FSM_IP_SET_IP_C1:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_C2;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_B3;
      } else if (btn == BTN_UP) {
	if (conf_ip[2] <= 155) {
	    conf_ip[2] += 100;
	} else
	  conf_ip[2] = 255;
      } else if (btn == BTN_DOWN) {
	if (conf_ip[2] >= 100) {
	    conf_ip[2] -= 100;
	} else
	  conf_ip[2] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_C2:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_C3;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_C1;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[2] <= 245) {
	    conf_ip[2] += 10;
	  } else
	    conf_ip[2] = 255;
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[2] >= 10) {
	    conf_ip[2] -= 10;
	  } else
	    conf_ip[2] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_C3:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_D1;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_C2;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[2] < 255) {
	    conf_ip[2] += 1;
	  }
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[2] > 0) {
	    conf_ip[2] -= 1;
	  }
      }
      break;
  case UI_FSM_IP_SET_IP_D1:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_D2;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_C3;
      } else if (btn == BTN_UP) {
	if (conf_ip[3] <= 155) {
	    conf_ip[3] += 100;
	} else
	  conf_ip[3] = 255;
      } else if (btn == BTN_DOWN) {
	if (conf_ip[3] >= 100) {
	    conf_ip[3] -= 100;
	} else
	  conf_ip[3] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_D2:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_IP_D3;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_D1;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[3] <= 245) {
	    conf_ip[3] += 10;
	  } else
	    conf_ip[3] = 255;
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[3] >= 10) {
	    conf_ip[3] -= 10;
	  } else
	    conf_ip[3] = 0;
      }
      break;
    case UI_FSM_IP_SET_IP_D3:
      if (btn == BTN_SET) {
	fsmState = UI_FSM_IP_SET_MASK;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_D2;
      } else if (btn == BTN_UP) {
       	  if (conf_ip[3] < 255) {
	    conf_ip[3] += 1;
	  }
      } else if (btn == BTN_DOWN) {
	  if (conf_ip[3] > 0) {
	    conf_ip[3] -= 1;
	  }
      }
      break;
    case UI_FSM_IP_SET_MASK:
      if (btn == BTN_SET) {
	//save settings
	applyNetworkSettings();
	fsmState = UI_FSM_IP;
      } else if (btn == BTN_BCK) {
	fsmState = UI_FSM_IP_SET_IP_D3;
      } else if (btn == BTN_UP) {
	if (conf_netmask < 30) {
	  conf_netmask++;
	}
      } else if (btn == BTN_DOWN) {
	if (conf_netmask > 0) {
	  conf_netmask--;
	}
      }
      break;
  } 

    
    updateDisplay();
  
}

int getBtnStatus() {
  int status = 0;
  status |= bcm2835_gpio_lev(GPIO_BTN_0) << 0;
  status |= bcm2835_gpio_lev(GPIO_BTN_1) << 1;
  status |= bcm2835_gpio_lev(GPIO_BTN_2) << 2;
  status |= bcm2835_gpio_lev(GPIO_BTN_3) << 3;

  return status;
}


int getBtnSaveStatus() {
  int status = 0;

  //There must have been a rising edge
  status |= bcm2835_gpio_eds(GPIO_BTN_0) << 0;
  status |= bcm2835_gpio_eds(GPIO_BTN_1) << 1;
  status |= bcm2835_gpio_eds(GPIO_BTN_2) << 2;
  status |= bcm2835_gpio_eds(GPIO_BTN_3) << 3;

  bcm2835_gpio_set_eds(GPIO_BTN_0);
  bcm2835_gpio_set_eds(GPIO_BTN_1);
  bcm2835_gpio_set_eds(GPIO_BTN_2);
  bcm2835_gpio_set_eds(GPIO_BTN_3);
  
  //and the button must be down for 6 extra ms
  int i;
  for (i = 0; i < 6; i++) {
    delay(1);
    status &= getBtnStatus();
  }

 
  
  return status;
}


int initUI() {
  if (initDisplay()) {
    return 1;
  }
  if (initBckl()) {
    return 1;
  }
  if (initBtn()) {
    return 1;
  }


  
  lcd_clear_screen();
  lcd_set_cursor(LINE1_START);
  lcd_send_string("    WELCOME     ");
  bcklOn();
  CPULoadInit();
  return 0;
}

void updateUI() {
  processBcklTimeout();
  int b = blink;
  processBlink();
  if ((fsmState == UI_FSM_IP_SET_IP_A1
       || fsmState == UI_FSM_IP_SET_IP_A2
       || fsmState == UI_FSM_IP_SET_IP_A3
       || fsmState == UI_FSM_IP_SET_IP_B1
       || fsmState == UI_FSM_IP_SET_IP_B2
       || fsmState == UI_FSM_IP_SET_IP_B3
       || fsmState == UI_FSM_IP_SET_IP_C1
       || fsmState == UI_FSM_IP_SET_IP_C2
       || fsmState == UI_FSM_IP_SET_IP_C3
       || fsmState == UI_FSM_IP_SET_IP_D1
       || fsmState == UI_FSM_IP_SET_IP_D2
       || fsmState == UI_FSM_IP_SET_IP_D3) && b != blink) {
    updateDisplay();
  }
  int status = getBtnSaveStatus();
  if (status) { //some btn was pressed
    if (status & BTN_BCK) {
      processBTN(BTN_BCK);
    } else if (status & BTN_SET) {
      processBTN(BTN_SET);
    } else if (status & BTN_UP) {
      processBTN(BTN_UP);
    } else if (status & BTN_DOWN) {
      processBTN(BTN_DOWN);
    }
  }
}
