
# Short project overview:

The IDN-Raspi-Laser project maybe considered as a blueprint of source code for an IDN consumer, interfacing to a DAC for X/Y + R,G,B via SPI.

## Folder contents:

1. /hardware - data for PCB with DAC as HAT for Raspberry Pi 3B, incl. a case design for 3D printout

2. /software - C source code for use on Raspberry Pi, using BCM235 library for SPI interfacing

3. /docs - some documentation, including the original source (German Bachelor thesis), links to ILDA Tech Awards in relation to the project

## Project history:

**IMPORTANT: Further development of this project has ceased!** A follow-up project used the C-based source code of the IDN-Raspi-Laser as a basis,
adapted to the object-oriented C++ programming language and added further modules for other DACs.

Link to the **follow-up project** [OpenIDN-Laser-DAC-Framework](https://gitlab.com/laser_light_lab_uni_bonn/OpenIDN-Laser-DAC-Framework) on gitlab.com.

The student **Christian Windeck** developed and implemented the IDN-Raspi-Laser prototype in a project group in 2018 (mandatory 
project and programming course in the study program of Bachelor Informatik at Uni Bonn)
and also continued work on the project in his Bachelor thesis at the Laser & Light Lab of Uni Bonn in 2018 and 2019. 
The Bachelor thesis (in German language) and some more documentation is in the folder /docs.

Finally, Christian created three prototype pieces of the IDN-Raspi-Laser, with the PCB with DACs and a case created for 3D printout.


## More information:

The source code is written in C programming language, running on
standard Raspbian OS, interfacing to an 8 channel DAC (for X+Y+R+G+B
with 3 channels spare) via a BCM2835 library using SPI. It has an ISP
DB25 output to a laser projector. The DAC in our prototype is an Analog Devices AD5668 with 8 channels and 16 bit resolution.

Maybe this project would be helpful for programming IDN consumer functionality
also on other hardware. Our project basically has two parts:

- PART 1: the part receiving IDN messages from the network, looking at
the IDN configuration, parsing the IDN dictionary and extracting the
sample values of X and Y and the three colors R,G,B and passing these
into a sample buffer.

- It supports both the IDN discrete graphics mode (aka frame) and the IDN
continous graphics mode (aka wave) and works with IDN capable software
(as IDN producer) and also when getting an IDN wave stream from software
or a converter like ISP StageFeed (Dexlogic).

- PART 2: the real-time part, it takes sample by sample from the buffer
and takes care of passing these via SPI to the DAC in correct timing
(according to the scan speed, or more specific according to IDN timing).
In frame mode, scanning the samples of a frame maybe repeated if no new
frame arrived in the meantime. In wave mode, the sample buffer never
should go empty which depends on some factors, network delay and jitter
among others. Our Raspberry Pi implementation may not be with best
performance and reliable, but it is not real-time optimized in any way,
just using an off-the-shelf Raspbian operating system with standard
process scheduler.

Maybe an existing real-time part (from sample buffer to DAC) of
Etherdream, Shownet,... maybe reused with slight adaptations, and the
"part 1" (receiving the IDN messages from the network) maybe used from
our Raspberry project.

A stripped-down version of the whole source code without SPI/BCM
interaction is also available. This version can be compiled on standard
Ubuntu/Linux and has some console debug output to show the performance
with incoming IDN streams. It maybe tested without DAC hardware (... but
has no laser output, of course).

## Feedback

We would be happy to receive feedback on your experience using our IDN tools on your side. You may send just comments, or also screenshots, screen capture video, or similar. 

Just drop an e-mail to laser-light-lab@uni-bonn.de


# **DISCLAIMER**

Please be aware that the IDN Stream Specification is an open protocol, like e.g. IP, the Internet Protocol, and its documentation is freely available to everybody (download at ILDA Technical Standards, https://www.ilda.com/technical.htm ). "_The IDN-Stream standard describes the encoding of laser show artwork into digital data streams._"

The specification and also manifold hardware and software elements have been implemented by volunteers, who are interested and motivated to foster the development of the open and publicly available components of the ILDA Digital Network. See more at http://ilda-digital.com

Up to now, no effort has been spent into encrypting or authenticating the IDN stream - as maybe known from other media types. 

When using our IDN tools, the copyrights of the artists who have created laser shows or laser show content have to be respected, as well as possible license agreements of the laser show system that is used together with our IDN tools.

**The University of Bonn / the Laser & Light Lab is not liable for damages or copyright/license infringements coming from third person's or third parties' use of the IDN prototype software provided via these gitlab.com groups or project repositories.**


