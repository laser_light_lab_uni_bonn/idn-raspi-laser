
## Documentation on IDN-Raspi-Laser (and ILDA IDN Awards)

- **ILDA 2023 Second Place, Fenning Technical Achievement (IDN)**

IDN-Interface Implementation Into a Regular PHAENON accurate; LaserAnimation Sollinger + University of Bonn, Institute of Computer Science 4 (Laser & Light Lab)

[ILDA 2023 Awards booklet, see page 56](https://www.ilda.com/resources/Awards/2023/2023-Awards-Booklet-05-post-Conference.pdf)

The IDN-Raspi-Laser project has helped LaserAnimation Sollinger during the year 2022 as a blue-print of an IDN consumer for their 
IDN-Interface Implementation in the firmware of the PHAENON accurate projector. The IDN firmware update capability for the PHAENON 
projector has been presented and demonstrated at the annual ILDA conference November 2022 in London. It was then entered to
the ILDA awards for 2023.

- **ILDA 2019 First Place, Fenning Technical Achievement (IDN)**

IDN Consumer Prototypes; University of Bonn, Institute of Computer Science 4 (Laser and Light Lab)

[ILDA 2019 Awards booklet, see page 62](https://www.ilda.com/resources/Awards/2019-Awards-booklet-04.pdf)

The original project itself received an ILDA award in the categroy Fenning Technical Achievement (IDN) in 2019. More details can be
found in the demo publication at IEEE LCN 2018, link at the bottom of this page.


Click on the thumbnail(s) below to go to the ILDA Award booklets (left to 2019, right to 2023):

<kbd>
<a href="https://www.ilda.com/resources/Awards/2019-Awards-booklet-04.pdf" target="_blank">
<img src="2019-ILDA-Award_IDN-Consumer-Prototypes.png" alt="Quick demo video (YouTube)" width="400" height="600" border="10" />
</a>
</kbd>
<kbd>
<a href="https://www.ilda.com/resources/Awards/2023/2023-Awards-Booklet-05-post-Conference.pdf" target="_blank">
<img src="2023-ILDA-Award_IDN-Interface-Implementation.png" alt="Quick demo video (YouTube)" width="400" height="600" border="10" />
</a>
</kbd>

(left see page 62 in the PDF, right see page 56 in the PDF)



- **File 2019-02-16_Christian-Windeck-Bachelor-Thesis-IDN-Raspi-Laser.pdf**

(in GERMAN language ONLY, use tools to translate) [Link to PDF](2019-02-16_Christian-Windeck-Bachelor-Thesis-IDN-Raspi-Laser.pdf)

**Christian Windeck:**  
Evaluation und Optimierung einer ILDA Digital Network (IDN) Laserprojektorschnittstelle  
Bachelor Thesis, Laser & Light Lab, Institute of Computer Science, University of Bonn, Germany, February 2019  


- **File 2019-02-25_Christian-Windeck-Bachelor-Thesis-Slides-Final-Presentation.pdf**

(in GERMAN language ONLY, use tools to translate) [Link to PDF](2019-02-25_Christian-Windeck-Bachelor-Thesis-Slides-Final-Presentation.pdf)

**Christian Windeck:**  
Evaluation und Optimierung einer ILDA Digital Network (IDN) Laserprojektorschnittstelle  
Slides of Final Presentation/Defense of BA Thesis  
Laser & Light Lab, Institute of Computer Science, University of Bonn, Germany, February 2019  



- **File 2018-IEEE-LCN-Laser-IDN-Consumer-Prototype-Demo.pdf**

Demo at IEEE LCN 2018, [Link to PDF](2018-IEEE-LCN-Laser-IDN-Consumer-Prototype-Demo.pdf)

**Matthias Frank:**  
Low-cost Prototypes of IDN (ILDA Digital Network) Consumers for ILDA Laser Projector and DMX512 Service,  
Demo at the 43rd IEEE Conference on Local Computer Networks (LCN), October 1-4, 2018, Chicago, USA  

https://www.ieeelcn.org/prior/LCN43/Program_demos.html




